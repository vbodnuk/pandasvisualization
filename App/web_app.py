import json

from flask import Flask, render_template

from DataManipulation.PandasManipulation import Test

app = Flask(__name__)


@app.route('/')
def data():
    a = Test.top_com_by_author()
    ch = a.to_json(orient='records')
    data = json.dumps(ch)
    return render_template('index.html', data=data)