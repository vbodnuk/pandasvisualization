import pandas as pd

class Test:


   # join 2 files
   @staticmethod
   def join_file():
      fl1 = pd.read_csv("/home/vb/PycharmProjects/Test/issues.csv")
      fl2 = pd.read_csv("/home/vb/PycharmProjects/Test/commits.csv")
      result = fl2.merge(fl1, on=["key"], how='inner')
      result.to_csv("final.csv")

   #sort by column created
   @staticmethod
   def sort_by():
      fl3 = pd.read_csv("/home/vb/PycharmProjects/Test/final.csv")
      fl3.sort_values(by=['created'])

   #groupby and print top10 commits by author
   @staticmethod
   def top_com_by_author():
      fl3 = pd.read_csv("/home/vb/PycharmProjects/Test/final.csv")
      gr = fl3.groupby('author_name').size().reset_index(name='counts').sort_values('counts', ascending=False)[:10]
      gr2 = pd.DataFrame(gr)
      return gr2
      # print(gr2)

   #print all issuestyoe where values are Bug
   @staticmethod
   def value_Bug():
      fl3 = pd.read_csv("/home/vb/PycharmProjects/Test/final.csv")
      print(fl3.loc[fl3['issuetype'] =='Bug'])



   #print between 2 dates
   @staticmethod
   def betw_date():
      fl3 = pd.read_csv("/home/vb/PycharmProjects/Test/final.csv")
      mask = (fl3['created'] >= '2016-04-01') & (fl3['created'] <= '2016-10-01')
      print(fl3.loc[mask])


   #top 10 commits by length
   @staticmethod
   def top10_commits_len():
      fl3 = pd.read_csv("/home/vb/PycharmProjects/Test/final.csv")

      mess = []
      length = []

      for st in fl3['message_encoding']:
         mess.append(st)
         length.append(len(st))

      df = pd.DataFrame({'message_encoding': mess, 'length': length})
      df1 = df
      print(df1.sort_values("length", ascending=False)[:10])